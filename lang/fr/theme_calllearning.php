<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package   theme_calllearning
 * @copyright 2018 - Clément Jourdain (clement.jourdain@gmail.com) & Laurent David (lmedavid@gmail.com)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'CALL Learning Moodle Theme';
$string['choosereadme'] = 'CALL Learning Moodle Theme est un theme basé sur Boost (utilise Bootstrap 4).';

/* Settings */
$string['brading_title'] = 'Marque du thème';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Logo pour le theme';

$string['coverimagefp'] = 'Image de couverture pour la page de garde';
$string['coverimagefpdesc'] = 'Image de couverture pour la page de garde';

$string['coverimage'] = 'Image de couverture';
$string['coverimagedesc'] = 'Image de couverture pour toutes les autres pages';

/* Block positionning */
$string['region-side-pre'] = "Side Pre";
$string['region-main'] = "Main";

/* Copyright footer */
$string['sitecopyrightmessage'] = 'SAS CALL Learning';

/* Front page */
$string['front_page_section_title']  = 'Welcome to our site / Bienvenue !';
$string['front_page_section_content']  = '<p></p><p>Apprendre avec CALL Learning, c\'est ...</p><p></p><ol><li>Une offre de formation pratique et sur mesure par des formateurs expérimentés dans leur spécialités<br></li><li>Des formations adaptées à vos problématiques en communication orale et écrite<br></li><li>Une expertise professionnelle et une pédagogie adaptée<br></li><li>Une plateforme de e-learning à votre disposition pour apprendre à votre rythme !</li></ol><p>Merci de votre confiance.</p><p></p><p><br></p><p>&nbsp;</p><p></p>';

/* Legal */
$string['legal'] = 'Mention légales';