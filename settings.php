<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;// Main settings.

$name = 'theme_calllearning/branding_title';
$heading = new lang_string('brading_title', 'theme_calllearning');
$description = '';
$setting = new admin_setting_heading($name, $heading, $description);
$settings->add($setting);

// Cover image for front-page file setting
$name = 'theme_calllearning/coverimagefp';
$title = new lang_string('coverimagefp', 'theme_calllearning');
$description = new lang_string('coverimagefpdesc', 'theme_calllearning');
$opts = array('accepted_types' => array('.png', '.jpg', '.gif', '.webp', '.svg'));
$setting = new admin_setting_configstoredfile($name, $title, $description, 'coverimagefp', 0, $opts);
$setting->set_updatedcallback('theme_calllearning_process_site_branding');
$settings->add($setting);


// Cover image file setting.
$name = 'theme_calllearning/coverimage';
$title = new lang_string('coverimage', 'theme_calllearning');
$description = new lang_string('coverimagedesc', 'theme_calllearning');
$opts = array('accepted_types' => array('.png', '.jpg', '.gif', '.webp', '.svg'));
$setting = new admin_setting_configstoredfile($name, $title, $description, 'coverimage', 0, $opts);
$setting->set_updatedcallback('theme_calllearning_process_site_branding');
$settings->add($setting);

// Logo image file setting.
$name = 'theme_calllearning/logo';
$title = new lang_string('logo', 'theme_calllearning');
$description = new lang_string('logodesc', 'theme_calllearning');
$opts = array('accepted_types' => array('.png', '.jpg', '.gif', '.webp', '.svg'));
$setting = new admin_setting_configstoredfile($name, $title, $description, 'logo', 0, $opts);
$setting->set_updatedcallback('theme_calllearning_process_site_branding');
$settings->add($setting);
